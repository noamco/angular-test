// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBlDNu0I1dAFo4m2xJSa-DVRH7Nyhj3dqg",
    authDomain: "test2-3ced4.firebaseapp.com",
    databaseURL: "https://test2-3ced4.firebaseio.com",
    projectId: "test2-3ced4",
    storageBucket: "test2-3ced4.appspot.com",
    messagingSenderId: "166568215447",
    appId: "1:166568215447:web:e1a01ca13a548e2974fad2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
