import { PostsService } from './../posts.service';    
import { Posts } from '../interfaces/posts';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 

  Users$: User[];
  title:string;
  body:string;
  ans:string;
  posts$;

  Posts$:Observable<any[]>;
  userId:string;

  constructor(private postsservice:PostsService, public authservice:AuthService) { }
// --------------- push posts to Firestore ---------------------
  // myFunc(){
  //       for (let index = 0; index < this.posts.length; index++) {
  //             this.title = this.posts[index].title;
  //             this.body = this.posts[index].body;
  //             this.postsservice.addPosts(this.title,this.body);      
  //       }
  //       this.ans ="The data retention was successful"
  //     }

    // ngOnInit() {
  
   
    //   // this.Posts$ = this.postsrvice.getPosts(); 
    // // this.postsrvice.getUser()
    // // .subscribe(data =>this.Users$ = data );
    // // this.postsrvice.getBlogPost()
    // // .subscribe(data =>this.Posts$ = data );
  

   
    //   this.postsservice.getPosts().subscribe(posts =>{
    //     console.log(posts);
    //     this.posts=posts;
    //   })
    // }
  

    //-------------------- get posts from FireStore --------------
    // deletePost(id){
    //   this.postsservice.deletePost(this.userId,id)
    //   console.log(id);
    // }

    ngOnInit() {
       this.posts$ = this.postsservice.getPosts();
     
      // this.postsrvice.getPost()
      // .subscribe(data =>this.Posts$ = data );
      // this.postsrvice.getUsers()
      // .subscribe(data =>this.Users$ = data );
      // console.log("NgOnInit started")  
      // this.authservice.getUser().subscribe(
      //   user => {
      //     this.userId = user.uid;
      //     this.Posts$ = this.postsservice.getPosts(this.userId); 
      //   }
      // )
        
      }
    
    }
  
  