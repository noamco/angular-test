import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { error } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //  Get User or Null
  user: Observable<User | null>
  token: string;
  errorMessage :string;

  // אם מייצרים אתחול של משתנה , כדאי שזה יהיה בתוך הבנאי
  constructor(public afAuth:AngularFireAuth,
               private router:Router) { 
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }



  SignUp(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(user => {
        this.router.navigate (['/welcome'])
      })
  }


  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password).catch((error)=>alert(error.message)).then(user => {
      
      if (user){this.router.navigate (['/welcome'])}
      else {this.router.navigate (['/login'])
    // this.errorMessage=(error.message)
  }
    })
}
private logInErrorSubject = new Subject<string>();

     public getLoginErrors():Subject<string>{
            return this.logInErrorSubject;
     }

     
}
