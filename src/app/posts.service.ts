import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interfaces/posts';
import { User } from './interfaces/user';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class PostsService {

  // private ApiPosts = "https://jsonplaceholder.typicode.com/posts"
  // private ApiUser = "https://jsonplaceholder.typicode.com/users"


  UserCollection:AngularFirestoreCollection = this.db.collection('Users');
  PostCollection:AngularFirestoreCollection;

// --------------- get data from API------------------------
  // constructor(private _http: HttpClient) { }
  
  // getPosts(): Observable<Posts>
  // {
  //   return this._http.get<Posts>(`${this.ApiPosts}`); 
  // }
  // getUser()
  // {
  //   return this._http.get<User[]>(this.ApiUser);
  // }

  // --------------- push data to Firestore------------------------

  // constructor(public httpClient:HttpClient, private db:AngularFirestore) { }
  // getPosts(){
  //   return this.httpClient.get(this.ApiPosts);
  // }

  // addPosts(title:string, body:string){
  //   const post = {title:title, body:body,};
  //   this.db.collection('posts').add(post);
  // }
  
// --------------- get data from Firestore------------------------
constructor(private _http: HttpClient ,private db: AngularFirestore , private authService:AuthService) { }

 // Read
    getPosts(): Observable<any[]> {
        const ref = this.db.collection('posts');
        return ref.valueChanges({idField: 'id'});
     } 

    getPost(id:string):Observable<any>{
            return this.db.doc(`posts/${id}`).get();
    }

    // Create
    addPost(title:string , body:string, author:string){
      const post = { title:title , body:body , author:author};
      this.db.collection('posts').add(post);
    }

    // Update
   updatePost(id:string ,title:string , body:string ,author:string)
    {
        this.db.doc(`posts/${id}`).update( { title:title , body:body, author:author  } )
    }

// Delete
    // Posts = the name of the Collection
    deletePost(id:string)
      {
        this.db.doc(`posts/${id}`).delete();
      }

}
 
